import imagen from '../assets/imgen.png'

export default () => {
    const img = document.createElement("img");
    img.src = imagen;

    return img;
  };
  